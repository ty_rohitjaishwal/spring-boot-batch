package com.tyss.springbootbatch.dto;

import java.sql.Date;

import lombok.Data;

@Data
public class Employee {
	private String name;
	private Integer empId;
	private Date dateOfBirth;
	private Date dateOfJoining;
	private String mailId;
	private Integer managerId;
}
